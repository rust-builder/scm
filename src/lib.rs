//! SCM module definitions.
//!
//! These modules are used by Rust Builders to interact with different SCM
//! systems.
#![experimental]
#![allow(unstable)]
extern crate commandext;

pub mod git;
pub mod hg;
pub mod svn;

//! Defines the `HgCommand` type.
//!
//! `HgCommand` wraps some commonly used `hg` commands for use with buildable.
//! `HgCommand` also exposes `hg_cmd` allowing you to create you own `hg`
//! based commands that aren't pre-defined.
//!
//! # Examples
//! ```rust
//! # extern crate scm; extern crate commandext; fn main() {
//! //! Below is what the test workflow for hg looks like
//! use commandext::to_res;
//! use scm::hg::HgCommand;
//! use std::io;
//! use std::io::IoResult;
//! use std::io::fs::{File,PathExtensions};
//! use std::io::fs::{copy,mkdir_recursive,rmdir_recursive};
//! use std::os::getcwd;
//!
//! fn touch(path: &Path) -> IoResult<()> {
//!     if !path.exists() {
//!         File::create(path).and_then(|_| Ok(()))
//!     } else {
//!         Ok(())
//!     }
//! }
//!
//! fn tmp_repo(name: &str) -> String {
//!     let mut dir = String::from_str("/tmp/");
//!     dir.push_str(name);
//!     dir.push_str(".hg");
//!     dir
//! }
//!
//! fn init(hcmd: &HgCommand, name: &str) -> Result<u8,u8> {
//!     let repo = tmp_repo(name);
//!     hcmd.init(Some(vec![repo.as_slice()]), to_res())
//! }
//!
//! // Cleanup from any previous failed tests.
//! let tmp = Path::new("/tmp");
//! if tmp.join("hgtest").exists() {
//!     assert_eq!(Ok(()), rmdir_recursive(&tmp.join("hgtest")));
//! }
//! if tmp.join("hgtest.hg").exists() {
//!     assert_eq!(Ok(()), rmdir_recursive(&tmp.join("hgtest.hg")));
//! }
//!
//! // Initialize a bare repository
//! let mut hcmd = HgCommand::new();
//! hcmd.verbose(true);
//! assert_eq!(Ok(0), init(&hcmd, "hgtest"));
//!
//! // Make temp project dir.
//! let pt = Path::new("/tmp/hgtest");
//! assert_eq!(Ok(()), mkdir_recursive(&pt, io::USER_RWX));
//!
//! // Clone into the temp project dir.
//! hcmd.wd(pt.clone());
//! assert_eq!(Ok(0), hcmd.clone(
//!     Some(vec!["/tmp/hgtest.hg", "."]), to_res()));
//!
//! // Copy hgrc, touch README.md, add, and commit.
//! let hgrc = getcwd().unwrap().join("hgrc");
//! assert_eq!(Ok(()), copy(&hgrc, &pt.join(".hg").join("hgrc")));
//! assert_eq!(Ok(()), touch(&pt.join("README.md")));
//!
//! // Add, commit and push
//! assert_eq!(Ok(0), hcmd.add(Some(vec!["."]), to_res()));
//! assert_eq!(Ok(0),
//!            hcmd.commit(Some(vec!["-m", "Test commit"]), to_res()));
//! assert_eq!(Ok(0), hcmd.push(None, to_res()));
//!
//! // Create feature branch, and push to remote.
//! assert_eq!(Ok(0), hcmd.branch(Some(vec!["feature"]), to_res()));
//! assert_eq!(Ok(()), touch(&pt.join("blah")));
//!
//! // Add, commit and push
//! assert_eq!(Ok(0), hcmd.add(Some(vec!["."]), to_res()));
//! assert_eq!(Ok(0),
//!            hcmd.commit(Some(vec!["-m", "Test commit"]), to_res()));
//! assert_eq!(Ok(0), hcmd.push(Some(vec!["--new-branch"]), to_res()));
//!
//! // Cleanup
//! let tmp = Path::new("/tmp");
//! if tmp.join("hgtest").exists() {
//!     assert_eq!(Ok(()), rmdir_recursive(&tmp.join("hgtest")));
//! }
//! if tmp.join("hgtest.hg").exists() {
//!     assert_eq!(Ok(()), rmdir_recursive(&tmp.join("hgtest.hg")));
//! }
//! # }
//! ```
#![experimental]
use commandext::CommandExt;
use std::io::process::Command;

/// `HgCommand` type definition.
#[experimental]
pub struct HgCommand {
    wd: Option<Path>,
    env: Vec<(String,String)>,
    verbose: bool,
}

impl HgCommand {
    /// Create a new HgCommand.
    ///
    /// ```rust
    /// use scm::hg::HgCommand;
    ///
    /// let hcmd = HgCommand::new();
    /// ```
    pub fn new() -> HgCommand {
        HgCommand{ wd: None, env: Vec::new(), verbose: false }
    }

    /// Add a working directory to the HgCommand.
    ///
    /// ```rust
    /// use scm::hg::HgCommand;
    ///
    /// let mut hcmd = HgCommand::new();
    /// let wd = Path::new("/tmp");
    /// hcmd.wd(wd);  // Any following hg_cmd commands will execute in the
    ///               // '/tmp' directory.
    /// ```
    ///
    /// # Arguments
    /// `wd` - The directory to execute the HgCommand in.
    ///
    /// # Notes
    /// * By default, the HgCommand is executed in the working directory
    /// of the parent process.
    #[experimental]
    pub fn wd(&mut self, wd: Path) -> &mut HgCommand {
        self.wd = Some(wd);
        self
    }

    /// Add an environment variable to the HgCommand.
    ///
    /// ```rust
    /// use scm::hg::HgCommand;
    ///
    /// let mut hcmd = HgCommand::new();
    /// hcmd.env(("DEBUG", "true"));
    /// hcmd.env(("VERBOSE", "true"));
    /// ```
    ///
    /// # Arguments
    /// * `env` - A tuple representing the environment (key,value) pair.
    #[experimental]
    pub fn env(&mut self, env: (&str,&str)) -> &mut HgCommand {
        let (k,v) = env;
        self.env.push((k.to_string(), v.to_string()));
        self
    }

    /// Set the verbose flag for the HgCommand.
    ///
    /// ```rust
    /// use scm::hg::HgCommand;
    ///
    /// let mut hcmd = HgCommand::new();
    /// hcmd.verbose(true);
    /// ```
    ///
    /// # Arguments
    /// * `verbose` - true to enable, false to disable.
    ///
    /// # Notes
    /// * By default, verbose is disabled.
    /// * If you use the `to_procout` function as the execfn, this
    /// setting has no meaning and is ignored.
    #[experimental]
    pub fn verbose(&mut self, verbose: bool) -> &mut HgCommand {
        self.verbose = verbose;
        self
    }

    /// Create and execute the hg `subcommand` with the given `args`, returning
    /// type `T`.
    ///
    /// ```rust
    /// # extern crate scm; extern crate commandext; fn main() {
    /// use commandext::to_res;
    /// use scm::hg::HgCommand;
    ///
    /// let mut hcmd = HgCommand::new();
    /// hcmd.verbose(true);
    /// // Run 'hg status' and send the output to stdout/stderr.
    /// assert_eq!(Err(255), hcmd.hg_cmd("status", None, to_res()));
    /// # }
    /// ```
    ///
    /// # Arguments
    /// * `subcommand` - The hg subcommand, i.e. 'commit'.
    /// * `args` - A vector of arguments to pass to the command.
    /// * `execfn` - A closure that takes a Command and returns type `T`.
    ///
    /// # Notes
    /// * Any environment variables added to the `HgCommand` are appended
    /// to the runtime environment.  They do not replace the environment.
    /// * The verbose flag will have no effect, unless the `execfn` function
    /// uses it for verbose output (see `to_res` for an example).
    pub fn hg_cmd<T>(&self,
                     subcommand: &str,
                     args: Option<Vec<&str>>,
                     execfn: fn(cmd: Command) -> T) -> T {
        let mut cmd = CommandExt::new("hg");

        match self.wd {
            Some(ref dir) => { cmd.wd(dir); },
            None          => { ; },
        }

        for &(ref k, ref v) in self.env.iter() {
            cmd.env(k.as_slice(), v.as_slice());
        }

        cmd.header(self.verbose);
        cmd.arg(subcommand);

        match args {
            None    => { ; },
            Some(a) => { cmd.args(a.as_slice()); },
        }

        cmd.exec(execfn)
    }

    /// Call `hg_cmd` with "add" as the subcommand.
    ///
    /// # Arguments
    /// See `hg_cmd` documentation for argument explanation.
    #[experimental]
    pub fn add<T>(&self,
                  args: Option<Vec<&str>>,
                  execfn: fn(cmd: Command) -> T) -> T {
        self.hg_cmd("add", args, execfn)
    }

    /// Call `hg_cmd` with "branch" as the subcommand.
    ///
    /// # Arguments
    /// See `hg_cmd` documentation for argument explanation.
    #[experimental]
    pub fn branch<T>(&self,
                     args: Option<Vec<&str>>,
                     execfn: fn(cmd: Command) -> T) -> T {
        self.hg_cmd("branch", args, execfn)
    }

    /// Call `hg_cmd` with "clone" as the subcommand.
    ///
    /// # Arguments
    /// See `hg_cmd` documentation for argument explanation.
    #[experimental]
    pub fn clone<T>(&self,
                    args: Option<Vec<&str>>,
                    execfn: fn(cmd: Command) -> T) -> T {
        self.hg_cmd("clone", args, execfn)
    }

    /// Call `hg_cmd` with "commit" as the subcommand.
    ///
    /// # Arguments
    /// See `hg_cmd` documentation for argument explanation.
    #[experimental]
    pub fn commit<T>(&self,
                     args: Option<Vec<&str>>,
                     execfn: fn(cmd: Command) -> T) -> T {
        self.hg_cmd("commit", args, execfn)
    }

    /// Call `hg_cmd` with "init" as the subcommand.
    ///
    /// # Arguments
    /// See `hg_cmd` documentation for argument explanation.
    #[experimental]
    pub fn init<T>(&self,
                   args: Option<Vec<&str>>,
                   execfn: fn(cmd: Command) -> T) -> T {
        self.hg_cmd("init", args, execfn)
    }

    /// Call `hg_cmd` with "pull" as the subcommand.
    ///
    /// # Arguments
    /// See `hg_cmd` documentation for argument explanation.
    #[experimental]
    pub fn pull<T>(&self,
                   args: Option<Vec<&str>>,
                   execfn: fn(cmd: Command) -> T) -> T {
        self.hg_cmd("pull", args, execfn)
    }

    /// Call `hg_cmd` with "push" as the subcommand.
    ///
    /// # Arguments
    /// See `hg_cmd` documentation for argument explanation.
    #[experimental]
    pub fn push<T>(&self,
                   args: Option<Vec<&str>>,
                   execfn: fn(cmd: Command) -> T) -> T {
        self.hg_cmd("push", args, execfn)
    }

    /// Call `hg_cmd` with "update" as the subcommand.
    ///
    /// # Arguments
    /// See `hg_cmd` documentation for argument explanation.
    #[experimental]
    pub fn update<T>(&self,
                     args: Option<Vec<&str>>,
                     execfn: fn(cmd: Command) -> T) -> T {
        self.hg_cmd("update", args, execfn)
    }
}

#[cfg(test)]
mod test {
    use commandext::to_res;
    use super::HgCommand;
    use std::io;
    use std::io::IoResult;
    use std::io::fs::{File,PathExtensions};
    use std::io::fs::{copy,mkdir_recursive,rmdir_recursive};
    use std::os::getcwd;

    fn touch(path: &Path) -> IoResult<()> {
        if !path.exists() {
            File::create(path).and_then(|_| Ok(()))
        } else {
            Ok(())
        }
    }

    fn tmp_repo(name: &str) -> String {
        let mut dir = String::from_str("/tmp/");
        dir.push_str(name);
        dir.push_str(".hg");
        dir
    }

    fn init(hcmd: &HgCommand, name: &str) -> Result<u8,u8> {
        let repo = tmp_repo(name);
        hcmd.init(Some(vec![repo.as_slice()]), to_res())
    }

    #[test]
    fn test_hg_workflow() {
        // Cleanup from any previous failed tests.
        let tmp = Path::new("/tmp");
        if tmp.join("hgtest").exists() {
            assert_eq!(Ok(()), rmdir_recursive(&tmp.join("hgtest")));
        }
        if tmp.join("hgtest.hg").exists() {
            assert_eq!(Ok(()), rmdir_recursive(&tmp.join("hgtest.hg")));
        }

        // Initialize a bare repository
        let mut hcmd = HgCommand::new();
        hcmd.verbose(true);
        assert_eq!(Ok(0), init(&hcmd, "hgtest"));

        // Make temp project dir.
        let pt = Path::new("/tmp/hgtest");
        assert_eq!(Ok(()), mkdir_recursive(&pt, io::USER_RWX));

        // Clone into the temp project dir.
        hcmd.wd(pt.clone());
        assert_eq!(Ok(0), hcmd.clone(
            Some(vec!["/tmp/hgtest.hg", "."]), to_res()));

        // Copy hgrc, touch README.md, add, and commit.
        let hgrc = getcwd().unwrap().join("hgrc");
        assert_eq!(Ok(()), copy(&hgrc, &pt.join(".hg").join("hgrc")));
        assert_eq!(Ok(()), touch(&pt.join("README.md")));

        // Add, commit and push
        assert_eq!(Ok(0), hcmd.add(Some(vec!["."]), to_res()));
        assert_eq!(Ok(0),
                   hcmd.commit(Some(vec!["-m", "Test commit"]), to_res()));
        assert_eq!(Ok(0), hcmd.push(None, to_res()));

        // Create feature branch, and push to remote.
        assert_eq!(Ok(0), hcmd.branch(Some(vec!["feature"]), to_res()));
        assert_eq!(Ok(()), touch(&pt.join("blah")));

        // Add, commit and push
        assert_eq!(Ok(0), hcmd.add(Some(vec!["."]), to_res()));
        assert_eq!(Ok(0),
                   hcmd.commit(Some(vec!["-m", "Test commit"]), to_res()));
        assert_eq!(Ok(0), hcmd.push(Some(vec!["--new-branch"]), to_res()));

        // Cleanup
        let tmp = Path::new("/tmp");
        if tmp.join("hgtest").exists() {
            assert_eq!(Ok(()), rmdir_recursive(&tmp.join("hgtest")));
        }
        if tmp.join("hgtest.hg").exists() {
            assert_eq!(Ok(()), rmdir_recursive(&tmp.join("hgtest.hg")));
        }
    }
}

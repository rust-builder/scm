//! Defines the `GitCommand` type.
//!
//! `GitCommand` wraps some commonly used `git` commands for use with buildable.
//! `GitCommand` also exposes `git_cmd` allowing you to create you own `git`
//! based commands that aren't pre-defined.
//!
//! # Examples
//! ```rust
//! # extern crate scm; extern crate commandext; fn main() {
//! //! Below is what the test workflow for git looks like
//! use scm::git::GitCommand;
//! use commandext::to_res;
//! use std::io;
//! use std::io::{File,IoResult};
//! use std::io::fs::PathExtensions;
//! use std::io::fs::{mkdir_recursive,rmdir_recursive};
//!
//! fn touch(path: &Path) -> IoResult<()> {
//!     if !path.exists() {
//!         File::create(path).and_then(|_| Ok(()))
//!     } else {
//!         Ok(())
//!     }
//! }
//!
//! fn tmp_repo(name: &str) -> String {
//!     let mut dir = String::from_str("/tmp/");
//!     dir.push_str(name);
//!     dir.push_str(".git");
//!     dir
//! }
//!
//! fn init_bare(gcmd: &GitCommand, name: &str) -> Result<u8,u8> {
//!     let repo = tmp_repo(name);
//!     gcmd.init(Some(vec!["--bare", repo.as_slice()]), to_res())
//! }
//!
//! fn clone_into(gcmd: &GitCommand,
//!               name: &str,
//!               remote: Option<&str>) -> Result<u8,u8> {
//!     let repo = match remote {
//!         None    => tmp_repo(name),
//!         Some(r) => r.to_string(),
//!     };
//!     gcmd.clone(Some(vec![repo.as_slice(), name]), to_res())
//! }
//!
//!
//! // Cleanup from any previous failed tests.
//! let tmp = Path::new("/tmp");
//! if tmp.join("pulltest").exists() {
//!     assert_eq!(Ok(()), rmdir_recursive(&tmp.join("pulltest")));
//! }
//! if tmp.join("pulltest1").exists() {
//!     assert_eq!(Ok(()), rmdir_recursive(&tmp.join("pulltest1")));
//! }
//! if tmp.join("pulltest.git").exists() {
//!     assert_eq!(Ok(()), rmdir_recursive(&tmp.join("pulltest.git")));
//! }
//!
//! // Initialize a bare repository
//! let mut gcmd = GitCommand::new();
//! gcmd.verbose(true);
//! assert_eq!(Ok(0), init_bare(&gcmd, "pulltest"));
//!
//! // Make temp project dir.
//! let pt = Path::new("/tmp/pulltest");
//! assert_eq!(Ok(()), mkdir_recursive(&pt, io::USER_RWX));
//!
//! // Init as a git repo, and touch a file, and add to the index.
//! gcmd.wd(Path::new("/tmp/pulltest"));
//! assert_eq!(Ok(0), gcmd.init(None, to_res()));
//! assert_eq!(Ok(0), gcmd.remote(Some(vec!["add",
//!                                         "origin",
//!                                         "/tmp/pulltest.git"]),
//!                               to_res()));
//! assert_eq!(Ok(()), touch(&pt.join("README.md")));
//! assert_eq!(Ok(0), gcmd.add(Some(vec!["."]), to_res()));
//!
//! // Setup temporary config.
//! assert_eq!(Ok(0), gcmd.config(Some(vec!["user.email",
//!                                         "jason.g.ozias@gmail.com"]),
//!                               to_res()));
//! assert_eq!(Ok(0), gcmd.config(Some(vec!["user.name", "Jason Ozias"]),
//!                               to_res()));
//! assert_eq!(Ok(0), gcmd.config(Some(vec!["push.default", "simple"]),
//!                               to_res()));
//!
//! // Commit and push to master.
//! assert_eq!(Ok(0), gcmd.commit(Some(vec!["-m", "initial commit"]),
//!                               to_res()));
//! assert_eq!(Ok(0), gcmd.push(Some(vec!["-u", "origin", "master"]),
//!                             to_res()));
//!
//! // Create feature branch, and push to remote.
//! assert_eq!(Ok(0), gcmd.branch(Some(vec!["feature"]), to_res()));
//! assert_eq!(Ok(0), gcmd.checkout(Some(vec!["feature"]), to_res()));
//! assert_eq!(Ok(0), gcmd.push(Some(vec!["-u", "origin", "feature"]),
//!                             to_res()));
//!
//! // Checkout master.
//! assert_eq!(Ok(0), gcmd.checkout(Some(vec!["master"]), to_res()));
//!
//! // Pull from the remote.
//! gcmd.wd(Path::new("/tmp/pulltest"));
//! assert_eq!(Ok(0), gcmd.pull(None, to_res()));
//!
//! // Clone to another working directory and fetch from the remote.
//! gcmd.wd(Path::new("/tmp"));
//! assert_eq!(Ok(0), clone_into(&gcmd, "pulltest1", Some("pulltest")));
//! gcmd.wd(Path::new("/tmp/pulltest1"));
//! assert_eq!(Ok(0), gcmd.fetch(None, to_res()));
//! assert_eq!(Ok(0), gcmd.remote(Some(vec!["update"]), to_res()));
//! assert_eq!(Ok(0), gcmd.update_branch("master"));
//!
//! // Cleanup
//! let tmp = Path::new("/tmp");
//! if tmp.join("pulltest").exists() {
//!     assert_eq!(Ok(()), rmdir_recursive(&tmp.join("pulltest")));
//! }
//! if tmp.join("pulltest1").exists() {
//!     assert_eq!(Ok(()), rmdir_recursive(&tmp.join("pulltest1")));
//! }
//! if tmp.join("pulltest.git").exists() {
//!     assert_eq!(Ok(()), rmdir_recursive(&tmp.join("pulltest.git")));
//! }
//! # }
//! ```
#![experimental]
use commandext::{CommandExt,to_res,to_procout};
use std::collections::HashMap;
use std::io::File;
use std::io::process::Command;
use std::os::getcwd;
use self::HeadType::{NamedBranch,Detached};
use self::RefType::{Heads,Tags};

/// Supported git HEAD types.
#[experimental]
#[derive(PartialEq)]
enum HeadType {
    /// HEAD points to a named branch.
    ///
    /// The argument is the name of the branch.
    NamedBranch(String),
    /// HEAD is detached and points to a commit.
    Detached,
}

// Supported git reference types.
#[experimental]
#[derive(PartialEq)]
enum RefType {
    /// refs/heads
    Heads,
    /// refs/tags
    Tags,
}

/// `GitCommand` type definition.
#[experimental]
pub struct GitCommand {
    wd: Option<Path>,
    env: Vec<(String,String)>,
    verbose: bool,
}

impl GitCommand {
    /// Create a new GitCommand.
    ///
    /// ```rust
    /// use scm::git::GitCommand;
    ///
    /// let gcmd = GitCommand::new();
    /// ```
    #[experimental]
    pub fn new() -> GitCommand {
        GitCommand{ wd: None, env: Vec::new(), verbose: false }
    }

    /// Add a working directory to the GitCommand.
    ///
    /// ```rust
    /// use scm::git::GitCommand;
    ///
    /// let mut gcmd = GitCommand::new();
    /// let wd = Path::new("/tmp");
    /// gcmd.wd(wd);  // Any following gcmd commands will execute in the
    ///               // '/tmp' directory.
    /// ```
    ///
    /// # Arguments
    /// * `wd` - The directory to execute the GitCommand in.
    ///
    /// # Notes
    /// * By default, the GitCommand is executed in the working directory
    /// of the parent process.
    #[experimental]
    pub fn wd(&mut self, wd: Path) -> &mut GitCommand {
        self.wd = Some(wd);
        self
    }

    /// Add an environment variable to the GitCommand.
    ///
    /// ```rust
    /// use scm::git::GitCommand;
    ///
    /// let mut gcmd = GitCommand::new();
    /// gcmd.env(("DEBUG", "true"));
    /// gcmd.env(("VERBOSE", "true"));
    /// ```
    ///
    /// # Arguments
    /// * `env` - A tuple representing the environment (key,value) pair.
    #[experimental]
    pub fn env(&mut self, env: (&str,&str)) -> &mut GitCommand {
        let (k,v) = env;
        self.env.push((k.to_string(), v.to_string()));
        self
    }

    /// Set the verbose flag for the GitCommand.
    ///
    /// ```rust
    /// use scm::git::GitCommand;
    ///
    /// let mut gcmd = GitCommand::new();
    /// gcmd.verbose(true);
    /// ```
    ///
    /// # Arguments
    /// * `verbose` - true to enable, false to disable.
    ///
    /// # Notes
    /// * By default, verbose is disabled.
    /// * If you use the `to_procout` function as the execfn, this
    /// setting has no meaning and is ignored.
    #[experimental]
    pub fn verbose(&mut self, verbose: bool) -> &mut GitCommand {
        self.verbose = verbose;
        self
    }

    /// Create and execute the git `subcommand` with the given `args`, returning
    /// type `T`.
    ///
    /// ```rust
    /// # extern crate scm; extern crate commandext; fn main() {
    /// use commandext::to_res;
    /// use scm::git::GitCommand;
    ///
    /// let mut gcmd = GitCommand::new();
    /// gcmd.verbose(true);
    /// // Run 'git status' and send the output to stdout/stderr.
    /// assert_eq!(Ok(0), gcmd.git_cmd("status", None, to_res()));
    /// # }
    /// ```
    ///
    /// # Arguments
    /// * `subcommand` - The git subcommand, i.e. 'commit'.
    /// * `args` - A vector of arguments to pass to the command.
    /// * `execfn` - A closure that takes a Command and returns type `T`.
    ///
    /// # Notes
    /// * Any environment variables added to the `GitCommand` are appended
    /// to the runtime environment.  They do not replace the environment.
    /// * The verbose flag will have no effect, unless the `execfn` function
    /// uses it for verbose output (see `to_res` for an example).
    pub fn git_cmd<T>(&self, subcommand: &str,
                      args: Option<Vec<&str>>,
                      execfn: fn(cmd: Command) -> T) -> T {
        let mut cmd = CommandExt::new("git");

        match self.wd {
            Some(ref dir) => { cmd.wd(dir); },
            None          => { ; },
        }

        for &(ref k, ref v) in self.env.iter() {
            cmd.env(k.as_slice(), v.as_slice());
        }

        cmd.header(self.verbose);
        cmd.arg(subcommand);

        match args {
            None    => { ; },
            Some(a) => { cmd.args(a.as_slice()); },
        }

        cmd.exec(execfn)
    }

    /// Call `git_cmd` with "add" as the subcommand.
    ///
    /// # Arguments
    /// See `git_cmd` documentation for argument explanation.
    #[experimental]
    pub fn add<T>(&self,
                  args: Option<Vec<&str>>,
                  execfn: fn(cmd: Command) -> T) -> T {
        self.git_cmd("add", args, execfn)
    }

    /// Call `git_cmd` with "branch" as the subcommand.
    ///
    /// # Arguments
    /// See `git_cmd` documentation for argument explanation.
    #[experimental]
    pub fn branch<T>(&self,
                     args: Option<Vec<&str>>,
                     execfn: fn(cmd: Command) -> T)  -> T {
        self.git_cmd("branch", args, execfn)
    }

    /// Call `git_cmd` with "checkout" as the subcommand.
    ///
    /// # Arguments
    /// See `git_cmd` documentation for argument explanation.
    #[experimental]
    pub fn checkout<T>(&self,
                       args: Option<Vec<&str>>,
                       execfn: fn(cmd: Command) -> T) -> T {
        self.git_cmd("checkout", args, execfn)
    }

    /// Call `git_cmd` with "clone" as the subcommand.
    ///
    /// # Arguments
    /// See `git_cmd` documentation for argument explanation.
    #[experimental]
    pub fn clone<T>(&self,
                    args: Option<Vec<&str>>,
                    execfn: fn(cmd: Command) -> T) -> T {
        self.git_cmd("clone", args, execfn)
    }

    /// Call `git_cmd` with "commit" as the subcommand.
    ///
    /// # Arguments
    /// See `git_cmd` documentation for argument explanation.
    #[experimental]
    pub fn commit<T>(&self,
                     args: Option<Vec<&str>>,
                     execfn: fn(cmd: Command) -> T) -> T {
        self.git_cmd("commit", args, execfn)
    }

    /// Call `git_cmd` with "config" as the subcommand.
    ///
    /// # Arguments
    /// See `git_cmd` documentation for argument explanation.
    #[experimental]
    pub fn config<T>(&self,
                     args: Option<Vec<&str>>,
                     execfn: fn(cmd: Command) -> T) -> T {
        self.git_cmd("config", args, execfn)
    }

    /// Call `git_cmd` with "fetch" as the subcommand.
    ///
    /// # Arguments
    /// See `git_cmd` documentation for argument explanation.
    ///
    /// # Notes
    /// * If `None` is supplied to fetch, the following arguments are supplied
    /// to `git_cmd`: "--all -p --recurse-submodules=on-demand".  This causes
    /// the fetch to check all remotes and descend into changed submodules by
    /// default.
    #[experimental]
    pub fn fetch<T>(&self,
                    args: Option<Vec<&str>>,
                    execfn: fn(cmd: Command) -> T) -> T {
        let args = match args {
            None    => Some(vec!["--all",
                                 "-p",
                                 "--recurse-submodules=on-demand"]),
            Some(a) => Some(a),
        };
        self.git_cmd("fetch", args, execfn)
    }

    /// Call `git_cmd` with "init" as the subcommand.
    ///
    /// See `git_cmd` documentation for argument explanation.
    #[experimental]
    pub fn init<T>(&self,
                   args: Option<Vec<&str>>,
                   execfn: fn(cmd: Command) -> T) -> T {
        self.git_cmd("init", args, execfn)
    }

    /// Call `git_cmd` with "ls-remote" as the subcommand.
    ///
    /// See `git_cmd` documentation for argument explanation.
    #[experimental]
    pub fn ls_remote<T>(&self,
                        args: Option<Vec<&str>>,
                        execfn: fn(cmd: Command) -> T) -> T {
        self.git_cmd("ls-remote", args, execfn)
    }

    /// Call `git_cmd` with "pull" as the subcommand.
    ///
    /// See `git_cmd` documentation for argument explanation.
    #[experimental]
    pub fn pull<T>(&self,
                   args: Option<Vec<&str>>,
                   execfn: fn(cmd: Command) -> T) -> T {
        self.git_cmd("pull", args, execfn)
    }

    /// Call `git_cmd` with "push" as the subcommand.
    ///
    /// See `git_cmd` documentation for argument explanation.
    #[experimental]
    pub fn push<T>(&self,
                   args: Option<Vec<&str>>,
                   execfn: fn(cmd: Command) -> T) -> T {
        self.git_cmd("push", args, execfn)
    }

    /// Call `git_cmd` with "remote" as the subcommand.
    ///
    /// See `git_cmd` documentation for argument explanation.
    #[experimental]
    pub fn remote<T>(&self,
                     args: Option<Vec<&str>>,
                     execfn: fn(cmd: Command) -> T) -> T {
        self.git_cmd("remote", args, execfn)
    }

    /// Call `git_cmd` with "rev-parse" as the subcommand.
    ///
    /// See `git_cmd` documentation for argument explanation.
    #[experimental]
    pub fn rev_parse<T>(&self,
                        args: Option<Vec<&str>>,
                        execfn: fn(cmd: Command) -> T) -> T {
        self.git_cmd("rev-parse", args, execfn)
    }

    /// Call `git_cmd` with "submodule" as the subcommand.
    ///
    /// See `git_cmd` documentation for argument explanation.
    #[experimental]
    pub fn submodule<T>(&self,
                        args: Option<Vec<&str>>,
                        execfn: fn(cmd: Command) -> T) -> T {
        let args = match args {
            None    => Some(vec!("update")),
            Some(a) => Some(a),
        };
        self.git_cmd("submodule", args, execfn)
    }

    /// The function determines the current head type.
    fn head_type(&self) -> HeadType {
        match self.rev_parse(Some(vec!["--abbrev-ref", "HEAD"]), to_procout()) {
            Ok(o) => {
                let co = String::from_utf8_lossy(o.output.as_slice());
                let res = co.trim();

                if res == "HEAD" {
                    Detached
                } else {
                    NamedBranch(res.to_string())
                }
            },
            Err(e) => panic!("failed to execute process: {}", e),
        }
    }

    /// Is the given `name` at `remote` a reference type of `rt`?
    fn is_remote_x(&self, name: &str, remote: &str, rt: &RefType) -> bool {
        let mut args = Vec::new();

        if *rt == RefType::Heads {
            args.push("-h");
        } else {
            args.push("-t");
        }

        args.push("--exit-code");
        args.push(remote);
        args.push(name);

        self.ls_remote(Some(args), to_res()).is_ok()
    }

    /// Create a map of the remotes that have a reference `name`.
    fn remotes_by_type(&self, name: &str, rt: RefType) -> HashMap<String,bool> {
        let mut res = HashMap::new();
        let rem = match self.remote(None, to_procout()) {
            Ok(o) => String::from_utf8_lossy(o.output.as_slice()).into_owned(),
            Err(e) => panic!("failed to execute process: {}", e),
        };

        for remote in rem.lines() {
            res.insert(remote.to_string(), self.is_remote_x(name, remote, &rt));
        }

        res
    }

    /// This function fetches, checks out, and pulls the requested branch if
    /// necessary.
    ///
    /// # Arguments
    /// * `branch` - The branch you are requesting
    ///
    /// # Return Value
    /// * `Ok(0)` - success
    /// * `Err(1)` - failed to fetch from remotes
    /// * `Err(2)` - failed to locate the requested branch
    ///
    /// # Notes
    /// * The fetch checks all remotes and any changed submodules.
    /// * If the branch does not exist locally, all remotes will be checked for
    /// the requested branch. If more than one branch is found remotely the user
    /// user will be prompted to select one to checkout.
    /// * The pull will only occur if the checked out branch is not in a
    /// detached state.
    #[experimental]
    pub fn update_branch(&self, branch: &str) -> Result<u8,u8> {
        match self.fetch(None, to_res()) {
            Ok(_) => {
                if !(self.head_type() == NamedBranch(branch.to_string())) {
                    match self.checkout(Some(vec![branch]), to_res()) {
                        Ok(status) => {
                            let cwd = getcwd().unwrap();
                            let wd = match self.wd {
                                Some(ref wd) => wd,
                                None         => &cwd,
                            };
                            let dirty = wd.join("dirty");
                            let mut df = File::create(&dirty);
                            df.write(b"").unwrap();

                            if !(self.head_type() == Detached) {
                                self.pull(None, to_res())
                            } else {
                                Ok(status)
                            }
                        },
                        Err(_)     => {
                            println!("Remote Heads: {:?}",
                                     self.remotes_by_type(branch,
                                                          RefType::Heads));
                            println!("Remote Tags: {:?}",
                                     self.remotes_by_type(branch,
                                                          RefType::Tags));
                            Err(2)
                        },
                    }.and(self.submodule(Some(vec!["init"]), to_res()))
                        .and(self.submodule(None, to_res()))
                } else {
                    self.pull(None, to_res())
                        .and(self.submodule(Some(vec!["init"]), to_res()))
                        .and(self.submodule(None, to_res()))
                }
            },
            Err(_)     => Err(1),
        }
    }
}

#[cfg(test)]
mod test {
    use commandext::to_res;
    use super::GitCommand;
    use std::io;
    use std::io::{File,IoResult};
    use std::io::fs::PathExtensions;
    use std::io::fs::{mkdir_recursive,rmdir_recursive};

    fn touch(path: &Path) -> IoResult<()> {
        if !path.exists() {
            File::create(path).and_then(|_| Ok(()))
        } else {
            Ok(())
        }
    }

    fn tmp_repo(name: &str) -> String {
        let mut dir = String::from_str("/tmp/");
        dir.push_str(name);
        dir.push_str(".git");
        dir
    }

    fn init_bare(gcmd: &GitCommand, name: &str) -> Result<u8,u8> {
        let repo = tmp_repo(name);
        gcmd.init(Some(vec!["--bare", repo.as_slice()]), to_res())
    }

    fn clone_into(gcmd: &GitCommand,
                  name: &str,
                  remote: Option<&str>) -> Result<u8,u8> {
        let repo = match remote {
            None    => tmp_repo(name),
            Some(r) => r.to_string(),
        };
        gcmd.clone(Some(vec![repo.as_slice(), name]), to_res())
    }

    #[test]
    fn test_git_workflow() {
        // Cleanup from any previous failed tests.
        let tmp = Path::new("/tmp");
        if tmp.join("pulltest").exists() {
            assert_eq!(Ok(()), rmdir_recursive(&tmp.join("pulltest")));
        }
        if tmp.join("pulltest1").exists() {
            assert_eq!(Ok(()), rmdir_recursive(&tmp.join("pulltest1")));
        }
        if tmp.join("pulltest.git").exists() {
            assert_eq!(Ok(()), rmdir_recursive(&tmp.join("pulltest.git")));
        }

        // Initialize a bare repository
        let mut gcmd = GitCommand::new();
        gcmd.verbose(true);
        assert_eq!(Ok(0), init_bare(&gcmd, "pulltest"));

        // Make temp project dir.
        let pt = Path::new("/tmp/pulltest");
        assert_eq!(Ok(()), mkdir_recursive(&pt, io::USER_RWX));

        // Init as a git repo, and touch a file, and add to the index.
        gcmd.wd(Path::new("/tmp/pulltest"));
        assert_eq!(Ok(0), gcmd.init(None, to_res()));
        assert_eq!(Ok(0), gcmd.remote(Some(vec!["add",
                                                "origin",
                                                "/tmp/pulltest.git"]),
                                      to_res()));
        assert_eq!(Ok(()), touch(&pt.join("README.md")));
        assert_eq!(Ok(0), gcmd.add(Some(vec!["."]), to_res()));

        // Setup temporary config.
        assert_eq!(Ok(0), gcmd.config(Some(vec!["user.email",
                                                "jason.g.ozias@gmail.com"]),
                                      to_res()));
        assert_eq!(Ok(0), gcmd.config(Some(vec!["user.name", "Jason Ozias"]),
                                      to_res()));
        assert_eq!(Ok(0), gcmd.config(Some(vec!["push.default", "simple"]),
                                      to_res()));

        // Commit and push to master.
        assert_eq!(Ok(0), gcmd.commit(Some(vec!["-m", "initial commit"]),
                                      to_res()));
        assert_eq!(Ok(0), gcmd.push(Some(vec!["-u", "origin", "master"]),
                                    to_res()));

        // Create feature branch, and push to remote.
        assert_eq!(Ok(0), gcmd.branch(Some(vec!["feature"]), to_res()));
        assert_eq!(Ok(0), gcmd.checkout(Some(vec!["feature"]), to_res()));
        assert_eq!(Ok(0), gcmd.push(Some(vec!["-u", "origin", "feature"]),
                                    to_res()));

        // Checkout master.
        assert_eq!(Ok(0), gcmd.checkout(Some(vec!["master"]), to_res()));

        // Pull from the remote.
        gcmd.wd(Path::new("/tmp/pulltest"));
        assert_eq!(Ok(0), gcmd.pull(None, to_res()));

        // Clone to another working directory and fetch from the remote.
        gcmd.wd(Path::new("/tmp"));
        assert_eq!(Ok(0), clone_into(&gcmd, "pulltest1", Some("pulltest")));
        gcmd.wd(Path::new("/tmp/pulltest1"));
        assert_eq!(Ok(0), gcmd.fetch(None, to_res()));
        assert_eq!(Ok(0), gcmd.remote(Some(vec!["update"]), to_res()));
        assert_eq!(Ok(0), gcmd.update_branch("master"));

        // Cleanup
        let tmp = Path::new("/tmp");
        if tmp.join("pulltest").exists() {
            assert_eq!(Ok(()), rmdir_recursive(&tmp.join("pulltest")));
        }
        if tmp.join("pulltest1").exists() {
            assert_eq!(Ok(()), rmdir_recursive(&tmp.join("pulltest1")));
        }
        if tmp.join("pulltest.git").exists() {
            assert_eq!(Ok(()), rmdir_recursive(&tmp.join("pulltest.git")));
        }
    }
}

//! Defines the `SvnCommand` type.
//!
//! `SvnCommand` wraps some commonly used `svn` commands for use with buildable.
//!
//! # Examples
//! ```rust
//! use scm::svn::{SvnCommand};
//!
//! let svn_cmd = SvnCommand::new();
//! ```
#![experimental]
use commandext::CommandExt;
use std::io::process::Command;

/// `SvnCommand` type definition.
#[experimental]
pub struct SvnCommand {
    wd: Option<Path>,
    env: Vec<(String,String)>,
    verbose: bool,
}

impl SvnCommand {
    /// Create a new SvnCommand.
    ///
    /// ```rust
    /// use scm::svn::SvnCommand;
    ///
    /// let scmd = SvnCommand::new();
    /// ```
    pub fn new() -> SvnCommand {
        SvnCommand{ wd: None, env: Vec::new(), verbose: false }
    }

    /// Add a working directory to the SvnCommand.
    ///
    /// ```rust
    /// use scm::svn::SvnCommand;
    ///
    /// let mut scmd = SvnCommand::new();
    /// let wd = Path::new("/tmp");
    /// scmd.wd(wd);  // Any following svn_cmd commands will execute in the
    ///               // '/tmp' directory.
    /// ```
    ///
    /// # Arguments
    /// `wd` - The directory to execute the SvnCommand in.
    ///
    /// # Notes
    /// * By default, the SvnCommand is executed in the working directory
    /// of the parent process.
    #[experimental]
    pub fn wd(&mut self, wd: Path) -> &mut SvnCommand {
        self.wd = Some(wd);
        self
    }

    /// Add an environment variable to the SvnCommand.
    ///
    /// ```rust
    /// use scm::svn::SvnCommand;
    ///
    /// let mut scmd = SvnCommand::new();
    /// scmd.env(("DEBUG", "true"));
    /// scmd.env(("VERBOSE", "true"));
    /// ```
    ///
    /// # Arguments
    /// * `env` - A tuple representing the environment (key,value) pair.
    #[experimental]
    pub fn env(&mut self, env: (&str,&str)) -> &mut SvnCommand {
        let (k,v) = env;
        self.env.push((k.to_string(), v.to_string()));
        self
    }

    /// Set the verbose flag for the SvnCommand.
    ///
    /// ```rust
    /// use scm::svn::SvnCommand;
    ///
    /// let mut scmd = SvnCommand::new();
    /// scmd.verbose(true);
    /// ```
    ///
    /// # Arguments
    /// * `verbose` - true to enable, false to disable.
    ///
    /// # Notes
    /// * By default, verbose is disabled.
    /// * If you use the `to_procout` function as the execfn, this
    /// setting has no meaning and is ignored.
    #[experimental]
    pub fn verbose(&mut self, verbose: bool) -> &mut SvnCommand {
        self.verbose = verbose;
        self
    }

    /// Create and execute the svn `subcommand` with the given `args`, returning
    /// type `T`.
    ///
    /// ```rust
    /// # extern crate scm; extern crate commandext; fn main() {
    /// use commandext::to_res;
    /// use scm::svn::SvnCommand;
    ///
    /// let mut scmd = SvnCommand::new();
    /// scmd.verbose(true);
    /// // Run 'svn status' and send the output to stdout/stderr.
    /// assert_eq!(Ok(0), scmd.svn_cmd("status", None, to_res()));
    /// # }
    /// ```
    ///
    /// # Arguments
    /// * `subcommand` - The svn subcommand, i.e. 'commit'.
    /// * `args` - A vector of arguments to pass to the command.
    /// * `execfn` - A closure that takes a Command and returns type `T`.
    ///
    /// # Notes
    /// * Any environment variables added to the `SvnCommand` are appended
    /// to the runtime environment.  They do not replace the environment.
    /// * The verbose flag will have no effect, unless the `execfn` function
    /// uses it for verbose output (see `to_res` for an example).
    pub fn svn_cmd<T>(&self, subcommand: &str,
                      args: Option<Vec<&str>>,
                      execfn: fn(cmd: Command) -> T) -> T {
        let mut cmd = CommandExt::new("svn");

        match self.wd {
            Some(ref dir) => { cmd.wd(dir); },
            None          => { ; },
        }

        for &(ref k, ref v) in self.env.iter() {
            cmd.env(k.as_slice(), v.as_slice());
        }

        cmd.header(self.verbose);
        cmd.arg(subcommand);

        match args {
            None    => { ; },
            Some(a) => { cmd.args(a.as_slice()); },
        }

        cmd.exec(execfn)
    }

    pub fn svnadmin_cmd<T>(&self, subcommand: &str,
                           args: Option<Vec<&str>>,
                           execfn: fn(cmd: Command) -> T) -> T {
        let mut cmd = CommandExt::new("svnadmin");

        match self.wd {
            Some(ref dir) => { cmd.wd(dir); },
            None          => { ; },
        }

        for &(ref k, ref v) in self.env.iter() {
            cmd.env(k.as_slice(), v.as_slice());
        }

        cmd.header(self.verbose);
        cmd.arg(subcommand);

        match args {
            None    => { ; },
            Some(a) => { cmd.args(a.as_slice()); },
        }

        cmd.exec(execfn)
    }

    /// Call `svn_cmd` with "co" as the subcommand.
    ///
    /// # Arguments
    /// See `svn_cmd` documentation for argument explanation.
    #[experimental]
    pub fn co<T>(&self,
                 args: Option<Vec<&str>>,
                 execfn: fn(cmd: Command) -> T) -> T {
        self.svn_cmd("co", args, execfn)
    }

    /// Call `svn_cmd` with "pull" as the subcommand.
    ///
    /// # Arguments
    /// See `svn_cmd` documentation for argument explanation.
    #[experimental]
    pub fn pull<T>(&self,
                   args: Option<Vec<&str>>,
                   execfn: fn(cmd: Command) -> T) -> T {
        self.svn_cmd("pull", args, execfn)
    }

    /// Call `svn_cmd` with "update" as the subcommand.
    ///
    /// # Arguments
    /// See `svn_cmd` documentation for argument explanation.
    #[experimental]
    pub fn update<T>(&self,
                     args: Option<Vec<&str>>,
                     execfn: fn(cmd: Command) -> T) -> T {
        self.svn_cmd("update", args, execfn)
    }
}

#[cfg(test)]
mod test {
    //use commandext::to_res;
    //use super::SvnCommand;

    #[test]
    fn test_svn_workflow() {
    }
}

# scm

Common SCM operations suitable for use by Rust Builders.

## Status
[![Build Status](https://travis-ci.org/rust-builder/scm.svg?branch=master)](travis)
